/*
 * Helios OS.
 * 2016/11/03
 * Sebastian Mountaniol
 */

#include <arch/registers.h>
#include <types.h>
#include "printk.h"

static void print_welcome(void)
{
    i32_t i = 0;
	i = printn("================================== \n", 36);
	i = printn("        Welcome to HelenOS         \n", 36);
	i = printn("================================== \n", 36);
}


static i32_t print_reg(i32_t reg_num)
{
    i32_t i = 0;
	char_t str[11] = {0,0,0,0,0,0,0,0,0,0,0};
	u32_t reg = 0;
	reg = READ_REG32(reg_num);
	if (0 != int32_to_hex_string(str, 10, reg)) {
		i = printn("Can't read register\n",20);
		return -1;
	}
	else {
		i = printn("Register:\n",10);
		i = printn(str, 10);
		i = printn("\n", 1);
	}
	return 0;
}

void c_entry(void)
{
    i32_t i = 0;
    char_t num[32] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	print_welcome();
	i = print_reg(REG_RW_IRQ_ENABLE);
	WRITE_REG32(REG_RW_IRQ_ENABLE, 0xFFFFFF);
	i = print_reg(REG_RW_IRQ_ENABLE);
    i = printk("Test \t %s: %d %x %d %c\n", "printk testing", 22, 56789,14, 'a');
    i = printn(num,32);
}


void __attribute__((interrupt("IRQ"))) do_irq()
{
    i32_t i = 0;
    i = printn("irq\n", 4);
}

void __attribute__((interrupt("FIQ"))) do_fiq()
{
    i32_t i = 0;
    i = printn("fiq\n", 4);
}

void __attribute__((interrupt("IRQ"))) data_aborted()
{
    i32_t i = 0;
    i = printn("abort\n", 4);
}


void __attribute__((interrupt("SWI"))) soft_int()
{
    i32_t i;
    #ifndef S_SPLINT_S /* splint protection */
    volatile u32_t int_num;
    asm("LDR r0, [lr, #-4]");
    asm("BIC r0, #0xFF000000");
    asm("MOV %0, r0":"=r"(int_num):);
    #endif
    i = printn("In software interrupt\n", 23);
    /* based on int_num, you can determine which system call is called */
}


