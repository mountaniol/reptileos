/*
 * Helios OS.
 * 2016/11/03
 * Sebastian Mountaniol
 */

#include <types.h>
#include <std.h>
#include "string.h"

/* Function:
 * void kzero(char str[], int len)
 * This is same function as bzero
 * The name is different to avoid warnings of lint and compiler
 * In:
 * char str[] - string to zero
 * int len - length of the string
 * Return:
 * nothing
 */

void kzero(char_t str[], i32_t len)
{
    len--;
    while (len >= 0)
        str[len--] = (char) 0;
}

/*
 * Function:
 * int kmemove(void *a, void *b, unsigned long size)
 * Move a memory region.
 * The memory can be overlapped.
 * IN:
 * void *dst - destination region
 * void *src - source region
 * unsigned long size - size to move
 * Return:
 * Success - 0
 * Error -1
 */
i32_t kmemmove(void *dst, void *src, u32_t size)
{
    if (dst == src || 0 == size )
        return 0;

    if (NULL == src || NULL == dst)
        return -1;

    do {
        *((char *) (unsigned long)dst + size) = *((char *) (unsigned long )dst + size);
        size--;
    } while (size > 0);

    return 0;
}

