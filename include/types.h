#ifndef _types_h_
#define _types_h_

/*
 * TODO: The platform-specifit types should be
 * implemented: reg_t which is compile to the
 * register length of the platform
 */
#include <arch/platform_types.h>

/* According to C standart the char type used for string only */
typedef char char_t;

typedef unsigned char u8_t;
typedef signed char i8_t;

typedef unsigned short u16_t;
typedef signed short i16_t;

typedef unsigned int u32_t;
typedef signed int i32_t;

typedef unsigned int size_t;

#endif /* _types_h_ */
