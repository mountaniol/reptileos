#ifndef __regisrers_h__
#define __regisrers_h__

#include <types.h>

#define WRITE_REG32(reg,val) \
	do{\
		(*(volatile reg_t *) reg) = (reg_t) val;\
	}while(0)

#define READ_REG32(reg) ((*(volatile reg_t *) reg))

#define REG_SERIAL 	0x101f1000

/* Interrupt regusters */

/*
 * See section 4.11.1:
 * http://infocenter.arm.com/help/topic/com.arm.doc.dui0224i/DUI0224I_realview_platform_baseboard_for_arm926ej_s_ug.pdf
 * And
 * ARM PrimeCell Vectored Interrupt Controller (PL192):
 * http://infocenter.arm.com/help/topic/com.arm.doc.ddi0273a/DDI0273.pdf
 *
 * ARM9EJ-S
 * Revision: r1p2
 * Technical Reference Manual
 * http://infocenter.arm.com/help/topic/com.arm.doc.ddi0222b/DDI0222.pdf
 */

/* Base address of IRQ registers */
#define REG_INT					0x10140000
/* Read: IRQ status register */
#define REG_R_IRQ_STATUS		0x10140000
/* Read: FIQ status register */
#define REG_R_FIQ_STATUS		0x10140004
/* Read: Raw interrupt status register */
#define REG_R_IRQ_RAW			0x10140008
/* Read/write: Interrupt select register */
#define REG_RW_IRQ_SELECT		0x1014000C
/* Read/write: Interrupt enable register */
#define REG_RW_IRQ_ENABLE		0x10140010
/* Write: Interrupt enable clear register */
#define REG_W_IRQ_ENABLE_CLR	0x10140014
/* Read/write: Software interrupt register */
#define REG_RW_SOFT_IRQ			0x10140018
/* Write: Software interrupt clear register */
#define REG_W_SOFT_IRQ_CLR		0x1014001C
/* Read/write: Protection enable register */
#define REG_RW_PROTECTION		0x10140020
/* Read/write: Vector address register */
#define REG_RW_VECTOR			0x10140030
/* Read/write: Default vector address register */
#define REG_RW_DEFAULT_VECTOR	0x10140034
/* Read/write: Vector address 0 to 15 registers */
#define REG_RW_VECTOR_0			0x10140100
#define REG_RW_VECTOR_N(num)	(REG_RW_VECTOR_0 + (num * 4))
/* Read/write: Vector control 0 to 15 registers */
#define REG_RW_VEC_CONTROL_0	0x10140200
#define REG_RW_VEC_CONTROL_N(num)	(REG_RW_VEC_CONTROL_0 + (num * 4))
/* Read/write: Test control registers */
#define REG_RW_TEST_PICITCR		0x10140300
#define REG_RW_TEST_PICITIP1	0x10140304
#define REG_RW_TEST_PICITIP2	0x10140308
#define REG_RW_TEST_PICITOP1	0x1014030C
#define REG_RW_TEST_PICITOP2	0x10140310
/* Read: Peripheral identification registers */
#define REG_R_PICPeriphID0		0x10140FE0
#define REG_R_PICPeriphID1		0x10140FE4
#define REG_R_PICPeriphID2		0x10140FE8
#define REG_R_PICPeriphID3		0x10140FEC
/* Read: PrimeCell identification registers */
#define REG_R_PRIMECELL0		0x10140FF0
#define REG_R_PRIMECELL1		0x10140FF4
#define REG_R_PRIMECELL2		0x10140FF8
#define REG_R_PRIMECELL3		0x10140FFC

/* Secondary interrupt controller */

/* Read: Status of interrupt (after mask) */
#define REG_R_SIC_STATUS		0x10003000
/* Read: Status of interrupt (before mask) */
#define REG_R_SIC_RAWSTAT		0x10003004
/* Read: Interrupt mask */
#define REG_R_SIC_ENABLE		0x10003008
/* Write: Set bits HIGH to enable the corresponding interrupt signals*/
#define REG_W_SIC_ENSET			0x1000300C
/* Read/write: Set software interrupt */
#define REG_RW_SIC_SOFTINTSET	0x10003010
/* Write: Clear software interrupt */
#define REG_W_SIC_SOFTINTCLR	0x10003014
/* Read: Read status of pass-through mask
 * (allows interrupt to pass directly to the
 * primary interrupt controller) */
#define REG_R_SIC_PICENABLE		0x10003020
/* Write: Set bits HIGH to set the corresponding
 * interrupt pass-through mask bits*/
#define REG_W_SIC_PICENSET		0x10003020
/* Write: Set bits HIGH to clear the corresponding
 * interrupt pass-through mask bits*/
#define REG_W_SIC_PICENCLR		0x10003024

/* Real Time Clock register */
#define REG_RTCLOCK				0x101E8000

#endif /* __regisrers_h__ */
