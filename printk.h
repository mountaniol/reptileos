#ifndef __printf_h__
#define __printf_h__
#define PRINTK_MAX_LEN	1024

i32_t printn(const char_t *format, const i32_t len, ...);
i32_t int32_to_hex_string(char_t *buf, i32_t len, u32_t val);
/* int int32_to_dec_string(char *buf, int len, unsigned int val); */
i32_t printk(const char_t format[], ...);
#endif /* __printf_h__ */
