.global _start
_start:
	b _reset
	ldr pc, _undefined_instruction
	ldr pc, _software_interrupt
	ldr pc, _prefetch_abort
	ldr pc, _data_abort
	ldr pc, _not_used
	ldr pc, _irq
	ldr pc, _fiq

_undefined_instruction:	.word undefined_instruction
_software_interrupt:	.word soft_int
_prefetch_abort:	.word prefetch_abort
_data_abort:		.word data_abort
_not_used:		.word not_used
_irq:			.word irq
_fiq:			.word fiq

_reset:
	ldr sp, =stack_top
	bl c_entry
	b .
undefined_instruction:
	b .

software_interrupt:
	b soft_int

prefetch_abort:
	b .

data_abort:
	b data_aborted

not_used:
	b .

irq:
	# A link register is a special-purpose register
	# which holds the address to return to
	# when a function call completes
	# Save Programm Counter to Link Register
	MOV lr, pc
	b do_irq
	# Restore Programm Counter from link Register
	MOV pc, lr

fiq:
	b do_fiq
