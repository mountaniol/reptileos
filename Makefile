TOPDIR=$(shell pwd)

# Recursion 
#wildcard=$(wildcard $1$2) $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2))

ALL_SOURCES=$(wildcard ./*.c)
ALL_SOURCES+=$(wildcard ./*.h)
ALL_SOURCES+=$(wildcard ./include/*.h)
ALL_SOURCES+=$(wildcard ./platforms/versatilepb/*.h)


#ALL_SOURCES=$(shell ./ -name '*.c')

# names: order is important!
OBJS=startup.o hello.o printk.o string.o reg.o
SRC=hello.c printk.c printk.h  string.h string.c
LSCRIPT=link.ld
OUT=hello

# tools
CC=arm-none-eabi-gcc
LD=arm-none-eabi-ld
AS=arm-none-eabi-as
OBJCOPY=arm-none-eabi-objcopy


# flags
AS_FLAGS=-mcpu=arm926ej-s -g
CC_FLAGS=-mcpu=arm926ej-s -g -pedantic -Wall
LD_FLAGS=
OBJCOPY_FLAGS=

# includes
INC=$(TOPDIR)/include/

# host tools
HCC=gcc
HLD=ld
HAS=as
HOBJCOPY=objcopy
HCFLAGS=-g -O1 -DHOSTDEBUG
HINC=include/


all: $(OBJS)
	@echo linking $(OUT).elf '<==' {$(OBJS)}
	@$(LD) -T $(LSCRIPT) $(OBJS) -o $(OUT).elf
	@echo creating $(OUT).bin '<==' $(OUT).elf
	@$(OBJCOPY) -O binary $(OUT).elf $(OUT).bin

clean:
	rm -f $(OBJS) $(OUT).elf $(OUT).bin

header:
	@echo '##################################################'
	@echo '###                                            ###'
	@echo '### Running the image in QEMU                  ###'
	@echo '### For exit:                                  ###'
	@echo '### Press Ctr + a and then x                   ###'
	@echo '###                                            ###'
	@echo '##################################################'

run: header
	qemu-system-arm -M versatilepb -m 128M -nographic -kernel hello.bin

debug: header
	qemu-system-arm -M versatilepb -m 128M -nographic -serial /dev/null -kernel hello.bin -s -S

retag:
	ctags -R ./

lint:
	echo $(ALL_SOURCES)
	splint +charint +trytorecover $(ALL_SOURCES) -I./include/
	#splint +charint +trytorecover $(SRC) -I./include/

unitest:
	$(HCC) -I$(HINC) $(HCFLAGS) unitest.c -c -o unitest.o
	$(HCC) -I$(HINC) $(HCFLAGS) print.c -c -o print.o
	$(HCC) -I$(HINC) $(HCFLAGS) unitest.o print.o -o unitest.out

%.o:%.c
	@echo building $@...
	@$(CC) -I$(INC) $(CC_FLAGS) -c -o $@ $<

%.o:%.s
	@echo building $@...
	@$(AS) $(AS_FLAGS) -o $@ $<

