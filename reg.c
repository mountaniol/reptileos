/*
 * Helios OS.
 * 2016/11/03
 * Sebastian Mountaniol
 */

/*
 * This file implements API to the system registers.
 * It implement the most important register manipulations functions                                                              .
 * and depends on low level flags from include/platform directory                                                                .
 * So it expected that every platform will redefine these registers                                                                                                                                                  .
 * with the same names.                                                                                                                              .
 */

#include "include/arch/registers.h"

void reg_interrupts_on(void)
{
    __asm volatile("SWI #0");
}

void reg_interrupts_off(void)
{
    __asm volatile("SWI #1");
}
