/*
 * Helios OS.
 * 2016/11/03
 * Sebastian Mountaniol
 */

#include <types.h>
#include <std.h>
#include <arch/registers.h>
#include "printk.h"
#include "string.h"


/*
 * Table for int to asci convertion.
 * Do not use it directly, use i8_to_acsi inline function.
 */
static const char_t g_str_hex[] = 	{ '0', '1', '2', '3',
    '4', '5', '6', '7',
    '8', '9', 'A', 'B',
    'C', 'D', 'E', 'F' };

/*
 * This function prevents wrong index when access to g_str_hex
 */
static inline char_t i8_to_acsi(u32_t num)
{
    return (num > sizeof(g_str_hex)? g_str_hex[0] : g_str_hex[num]); \
}

/* Basic test that characted is not a junk */
inline static i32_t test_char(const char_t c) {
    /* Printable symbols start from space and end with ~ */
    /* 15 == carriage return */
    if (((c >= ' ') & (c <= '~')) || c == '\n') return 0;
    return 1;
}

/*
 * Send a prefomatted string to the serial port
 * In:
 * const char_t *s - the string to be send
 * const i32_t len - maximum number of characters to send, must be > 0
 * Return:
 * Number of send characters on success, -1 on error
 */
#ifdef HOSTDEBUG
/* In case of host unitesting this function is empty */
static i32_t print_uart0(const char_t *s, const i32_t len) {
	return len;
}
#else
static i32_t print_uart0(const char_t *s, const i32_t len) {
    i32_t printed = 0;

    if (!s || len < 1) return -1;

    while (*s != '\0' && printed < len) {

        /* Test that current character is a legal symbol */
        if (0 != test_char(*s)) return printed;

        WRITE_REG32(REG_SERIAL, *s);
        s++;
        printed++;
    }
    return printed;
}
#endif
/*
 * Print string into UART
 * IN:
 * format  - string
 * len     - the string length
 * Return:
 * Success - number of printed characters
 * Error   - (-1)
*/
i32_t printn(const char_t *format, const i32_t len, ...) {
    return print_uart0(format, len);
}

/*
 * Convert 32 bit integer into a hex string
 * In:
 * char_t buf         - the output buffer, must be at least 10 characters
 * i32_t len          - length of the output buffer
 * u32_t val - inoput value to be converted
 * Success - number of chars printed in the head of the buf
 * Error - (-1)s
 */
i32_t int32_to_hex_string(char_t buf[], const i32_t len, u32_t val) {
    i32_t i = 0;
    i32_t ret = 10;
    if (NULL == buf || len < 10)
        return -1;

    buf[0] = '0';
    buf[1] = 'x';

	if (0 == val) {
		buf[2] = '0';
		buf[3] = '0';
		return 3;
	}

    for (i = 9; i > 1; i--) {
        buf[i] = i8_to_acsi(val & 0xF);
        val >>= 4;
    }

    /* Cut off leading zeros if any */

    for (i = 0; i < 8; i++) {
        if (buf[i+2] != '0')
            break;
    }

    if (i > 0) {
        i32_t j;
        for (j = 0; j <= 8 - i; j++)
            buf[2 + j] = buf[2 + j + i];

        buf[2 + j + i] = '\0';
        ret = 2 + j + i;
    }

    return ret;
}
/*
 * Convert 32 bit integer into decimal string
 * In:
 * char_t buf[]         - the output buffer, must be at least 10 characters
 * i32_t len          - length of the output buffer
 * u32_t val - input value to be converted
 * Return:
 * Success - number of chars printed
 * Error - (-1)
 */
static i32_t int32_to_dec_string(char_t buf[], const i32_t len, u32_t val) {
    i32_t i = 0;
    i32_t j = 0;
    if (NULL == buf)
        return -1;

    for (i = len - 1; i >= 0; i--) {
        buf[i] = i8_to_acsi(val % 10);
        val /= 10;

        if (0 == val) {
            /* we are finished.
               If it shorter then the received buf - relocate
               the string to the beginning of the buffer */
            if (0 != i) {
                for (j = 0; j <= len - i; j++) {
                    buf[j] = buf[i + j];
                }
                buf[j] = 0;
            }
            return j;
        }
    }
    return len - i;
}

/*
 * Print out string as it does printf
 * IN:
 * format     - formatted string including % modificators
 * additional - values of numbers to be printed
 * Supported % format:
 * %d         - print integer as decimal
 * %x         - print integer as hex
 * Return:
 * Success    - 0
 * Error      - (-1)
*/
i32_t printk(const char_t format[], ...) {
    i32_t *ap = (i32_t *)&format;
    i32_t cnt = 0;
    char_t str[32] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    i32_t i = 0;

    if (NULL == format)
        return -1;

    ap++;

    while ('\0' != format[cnt]) {
        if (format[cnt] == '%') {
            cnt++; /* move to the modificator after % */
            switch (format[cnt]){
            case 'd':
                kzero((char_t *)str,32);
                i = int32_to_dec_string(str, 32, (u32_t) *ap++);
                if (0 != i)
                    i = printn(str, i);
                break;
            case 'x':
                kzero( (char_t *) str,32);
                i = int32_to_hex_string(str, 32, (u32_t) *ap++);
                if (0 != i)
                    i = printn(str, i);
                break;
 
            case 'c': {
                i32_t z;
                z = i8_to_acsi((u32_t) *ap++);
                WRITE_REG32(REG_SERIAL, z);
                break;
                }
            case 's':
                i = printn((char_t *) *ap++, PRINTK_MAX_LEN);
                break;
            }
        }
        else /* A regulat character */
            WRITE_REG32(REG_SERIAL, format[cnt]);
        cnt++; /* next char */
    }
    return 0;
}
