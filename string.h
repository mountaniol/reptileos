/*
 * Helios OS.
 * 2016/11/03
 * Sebastian Mountaniol
 */

#ifndef _string_h_
#define _string_h_
#include <types.h>

void    kzero(char_t str[], i32_t len);
i32_t   kmemmove(void *dst, void *src, u32_t size);

#endif /* _string_h_ */
